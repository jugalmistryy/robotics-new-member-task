
void setup()
{
  Serial.begin(9600);
}

float vsense,vbatt;
int vpin=18,R1=10000,R2=2000;
void loop()
{ 
  vbatt=vbattRead();
  Serial.print("Battery Voltage: ");
  Serial.println(vbatt);

  if (vbatt<12)
    Serial.println("WARNING! Battery Voltage Low");

  if (vbatt>16.8)
    Serial.println("WARNING! Battery Voltage High");
    
 }

 float vbattRead()
 {
    vsense= (analogRead(vpin)*3.3)/1024;
    return vsense*(R1+R2)/R2;
 }
